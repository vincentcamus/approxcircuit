-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus@epfl.ch
--
-- Friday, May 16 2014
-- Version 1.0


------------------------------------------------------------------- STAGE 2 TOP ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.functions_pkg.ALL;

ENTITY ADD_stage IS
	GENERIC (
		ADDER_WIDTH : INTEGER;
		BLOCK_NB    : INTEGER;
		ADD_REG     : STRING);
	PORT (
		a       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		b       : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_specb : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		s_addb  : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_addb  : OUT STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0));
END ENTITY ADD_stage;

------------------------------------------------------------------- STAGE 2 TOP ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF ADD_stage IS

---------------------------------- COMPONENTS

	-- component ADD declaration
	COMPONENT ADD IS
		GENERIC (
			ADD_WIDTH : INTEGER);
		PORT (
			ai : IN  STD_LOGIC_VECTOR (ADD_WIDTH-1 DOWNTO 0);
			bi : IN  STD_LOGIC_VECTOR (ADD_WIDTH-1 DOWNTO 0);
			ci : IN  STD_LOGIC;
			so : OUT STD_LOGIC_VECTOR (ADD_WIDTH-1 DOWNTO 0);
			co : OUT STD_LOGIC);
	END COMPONENT;

---------------------------------- DESCRIPTION

BEGIN

	-- ADD block instantiations
	gen_ADD: FOR i IN BLOCK_NB-1 DOWNTO 0 GENERATE
		-- SPEC
		inst_ADD: ADD
		GENERIC MAP (
			char_to_int(ADD_REG(i+1)))
		PORT MAP (
			a(block_start(ADD_REG, i)+char_to_int(ADD_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)),
			b(block_start(ADD_REG, i)+char_to_int(ADD_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)),
			c_specb(i),
			s_addb(block_start(ADD_REG, i)+char_to_int(ADD_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)),
			c_addb(i));
	END GENERATE;

END ARCHITECTURE standard;

------------------------------------------------------------------- STAGE 2 ADD ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ADD IS
	GENERIC (
		ADD_WIDTH : INTEGER);
	PORT (
		ai : IN  STD_LOGIC_VECTOR (ADD_WIDTH-1 DOWNTO 0);
		bi : IN  STD_LOGIC_VECTOR (ADD_WIDTH-1 DOWNTO 0);
		ci : IN  STD_LOGIC;
		so : OUT STD_LOGIC_VECTOR (ADD_WIDTH-1 DOWNTO 0);
		co : OUT STD_LOGIC);
END ENTITY ADD;

------------------------------------------------------------------- STAGE 2 ADD ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF ADD IS

---------------------------------- SIGNALS

	SIGNAL s_big : STD_LOGIC_VECTOR(ADD_WIDTH DOWNTO 0);

---------------------------------- DESCRIPTION

BEGIN

	-- sum
	s_big <= STD_LOGIC_VECTOR(UNSIGNED('0' & ai)+UNSIGNED('0' & bi)+(ci & ""));
	
	-- boundaries
	so <= s_big(ADD_WIDTH-1 DOWNTO 0);
	co <= s_big(ADD_WIDTH);

END ARCHITECTURE standard;
