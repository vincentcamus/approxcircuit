-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus@epfl.ch
--
-- Friday, May 16 2014
-- Version 1.0


------------------------------------------------------------------- STAGE 3 TOP ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.functions_pkg.ALL;

ENTITY COMP_stage IS
	GENERIC (
		ADDER_WIDTH : INTEGER;
		BLOCK_NB    : INTEGER;
		ADD_REG     : STRING;
		GUESS_REG   : STRING;
		TEST_REG    : STRING;
		COR_REG     : STRING;
		RED_REG     : STRING);
	PORT (
		s_addb  : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_specb : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		c_addb  : IN  STD_LOGIC_VECTOR (BLOCK_NB-1 DOWNTO 0);
		s_compb : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		c_compb : OUT STD_LOGIC);
END ENTITY COMP_stage;

------------------------------------------------------------------- STAGE 3 TOP ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF COMP_stage IS

---------------------------------- COMPONENTS

	-- component COMP_S declaration
	COMPONENT COMP_S IS
		GENERIC (
			GUESS : CHARACTER;
			COR   : INTEGER;
			RED   : INTEGER);
		PORT (
			s_cor_in  : IN  STD_LOGIC_VECTOR (COR-1 DOWNTO 0);
			s_red_in  : IN  STD_LOGIC_VECTOR (RED-1 DOWNTO 0);
			c_specb   : IN  STD_LOGIC;
			c_addb    : IN  STD_LOGIC;
			s_cor_out : OUT STD_LOGIC_VECTOR (COR-1 DOWNTO 0);
			s_red_out : OUT STD_LOGIC_VECTOR (RED-1 DOWNTO 0));
	END COMPONENT;

	-- component COMP_N declaration
	COMPONENT COMP_N IS
		GENERIC (
			GUESS : CHARACTER;
			COR   : INTEGER);
		PORT (
			s_cor_in  : IN  STD_LOGIC_VECTOR (COR-1 DOWNTO 0);
			c_specb   : IN  STD_LOGIC;
			c_addb    : IN  STD_LOGIC;
			s_cor_out : OUT STD_LOGIC_VECTOR (COR-1 DOWNTO 0));
	END COMPONENT;

---------------------------------- SIGNALS

	-- inputs
	SIGNAL s_cor_i : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	SIGNAL s_red_i : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	-- final outputs
	SIGNAL s_cor_o : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	SIGNAL s_red_o : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	-- instance outputs
	SIGNAL s_cor_b : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	SIGNAL s_red_b : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);

---------------------------------- DESCRIPTION

BEGIN

	-- inputs/outputs
	s_cor_i <= c_addb(BLOCK_NB-1) & s_addb;
	s_compb <= s_red_o(ADDER_WIDTH-1 DOWNTO 0);
	c_compb <= s_red_o(ADDER_WIDTH);

	-- instance connections
	s_red_i <= s_cor_o;

	-- COMP block instantiations
	gen_COMP: FOR i IN BLOCK_NB-1 DOWNTO 1 GENERATE

		-- COMP standard (check if correction is possible)
		type_S: IF TEST_REG(i+1) = 'S' GENERATE
			inst_COMP_S: COMP_S
			GENERIC MAP (
				GUESS_REG(i+1),
				char_to_int(COR_REG(i+1)),
				char_to_int(RED_REG(i+1)))
			PORT MAP (
				s_cor_i(block_start(ADD_REG, i)+char_to_int(COR_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)),
				s_red_i(block_start(ADD_REG, i)-1 DOWNTO block_start(ADD_REG, i)-char_to_int(RED_REG(i+1))),
				c_specb(i),
				c_addb(i-1),
				s_cor_b(block_start(ADD_REG, i)+char_to_int(COR_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)),
				s_red_b(block_start(ADD_REG, i)-1 DOWNTO block_start(ADD_REG, i)-char_to_int(RED_REG(i+1))));
		END GENERATE;

		-- COMP no test (always correction, take care of overflow !)
		type_N: IF TEST_REG(i+1) = 'N' GENERATE
			inst_COMP_N: COMP_N
			GENERIC MAP (
				GUESS_REG(i+1),
				char_to_int(COR_REG(i+1)))
			PORT MAP (
				s_cor_i(block_start(ADD_REG, i)+char_to_int(COR_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)),
				c_specb(i),
				c_addb(i-1),
				s_cor_b(block_start(ADD_REG, i)+char_to_int(COR_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)));
		END GENERATE;

	END GENERATE;

	-- connection & buffering process
	PROCESS (s_cor_i, s_red_i, s_cor_b, s_red_b)
	BEGIN
		-- no COMP action default assignment
		s_cor_o <= s_cor_i;
           	s_red_o <= s_red_i;
		-- erasing default output
		FOR i IN BLOCK_NB-1 DOWNTO 1 LOOP
			-- correction buffer
			s_cor_o(block_start(ADD_REG, i)+char_to_int(COR_REG(i+1))-1 DOWNTO block_start(ADD_REG, i)) <=
			s_cor_b(block_start(ADD_REG, i)+char_to_int(COR_REG(i+1))-1 DOWNTO block_start(ADD_REG, i));
			-- redancing buffer
			s_red_o(block_start(ADD_REG, i)-1 DOWNTO block_start(ADD_REG, i)-char_to_int(RED_REG(i+1))) <=
			s_red_b(block_start(ADD_REG, i)-1 DOWNTO block_start(ADD_REG, i)-char_to_int(RED_REG(i+1)));
		END LOOP;
	END PROCESS;

END ARCHITECTURE standard;

------------------------------------------------------------------- STAGE 2 COMP_S ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY COMP_S IS
	GENERIC (
		GUESS : CHARACTER;
		COR   : INTEGER;
		RED   : INTEGER);
	PORT (
		s_cor_in  : IN  STD_LOGIC_VECTOR (COR-1 DOWNTO 0);
		s_red_in  : IN  STD_LOGIC_VECTOR (RED-1 DOWNTO 0);
		c_specb   : IN  STD_LOGIC;
		c_addb    : IN  STD_LOGIC;
		s_cor_out : OUT STD_LOGIC_VECTOR (COR-1 DOWNTO 0);
		s_red_out : OUT STD_LOGIC_VECTOR (RED-1 DOWNTO 0));
END ENTITY COMP_S;

------------------------------------------------------------------- STAGE 2 COMP_S ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF COMP_S IS

---------------------------------- SIGNALS

	SIGNAL c_err : STD_LOGIC;

---------------------------------- DESCRIPTION

BEGIN

	-- error detection
	c_err <= c_specb XOR c_addb;

	-- '0' or always-low guess - always +1 correction
	dir_0: IF (GUESS = '0' OR GUESS = 'G') GENERATE
		PROCESS (c_err, s_cor_in, s_red_in, c_addb, c_specb)
		BEGIN
			IF (c_err = '0')
			THEN -- no error
				s_cor_out <= s_cor_in;
				s_red_out <= s_red_in;
			ELSE -- error
				IF (s_cor_in = (COR-1 downto 0 => '1')) -- no correction possible		
				THEN
					-- no correction
					s_cor_out <= s_cor_in;
					-- compensation
					s_red_out <= (others => '1');
				ELSE
					-- correction
					s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)+1);
					-- no compensation
					s_red_out <= s_red_in;
				END IF;
			END IF;
		END PROCESS;
	END GENERATE;

	-- '1' guess - always -1 correction
	dir_1: IF GUESS = '1' GENERATE
		PROCESS (c_err, s_cor_in, s_red_in, c_addb, c_specb)
		BEGIN
			IF (c_err = '0')
			THEN -- no error
				s_cor_out <= s_cor_in;
				s_red_out <= s_red_in;
			ELSE -- error
				IF (s_cor_in = (COR-1 downto 0 => '0')) -- no correction possible		
				THEN
					-- no correction
					s_cor_out <= s_cor_in;
					-- compensation
					s_red_out <= (others => '0');
				ELSE
					-- correction
					s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)-1);
					-- no compensation
					s_red_out <= s_red_in;
				END IF;
			END IF;
		END PROCESS;
	END GENERATE;

	-- variable guess
	dir_v: IF (GUESS /= '0' AND GUESS /= 'G' AND GUESS /= '1') GENERATE
		PROCESS (c_err, s_cor_in, s_red_in, c_addb, c_specb)
		BEGIN
			IF (c_err = '0')
			THEN -- no error
				s_cor_out <= s_cor_in;
				s_red_out <= s_red_in;
			ELSE -- error
				IF (s_cor_in = (COR-1 downto 0 => c_addb)) -- no correction possible		
				THEN
					-- no correction
					s_cor_out <= s_cor_in;
					-- compensation
					s_red_out <= (others => c_addb);
				ELSE
					-- correction
					IF c_addb = '1'
					THEN -- too low error
						s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)+1);
					ELSE -- too high error
						s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)-1);
					END IF;
					-- no compensation
					s_red_out <= s_red_in;
				END IF;
			END IF;
		END PROCESS;
	END GENERATE;

END ARCHITECTURE standard;

------------------------------------------------------------------- STAGE 2 COMP_N ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY COMP_N IS
	GENERIC (
		GUESS : CHARACTER;
		COR   : INTEGER);
	PORT (
		s_cor_in  : IN  STD_LOGIC_VECTOR (COR-1 DOWNTO 0);
		c_specb   : IN  STD_LOGIC;
		c_addb    : IN  STD_LOGIC;
		s_cor_out : OUT STD_LOGIC_VECTOR (COR-1 DOWNTO 0));
END ENTITY COMP_N;

------------------------------------------------------------------- STAGE 2 COMP_N ENTITY ARCHITECTURE STANDARD

ARCHITECTURE standard OF COMP_N IS

---------------------------------- SIGNALS

	SIGNAL c_err : STD_LOGIC;

---------------------------------- DESCRIPTION

BEGIN

	-- error detection
	c_err <= c_specb XOR c_addb;

	-- '0' or always-low guess - always +1 correction
	dir_0: IF (GUESS = '0' OR GUESS = 'G') GENERATE
		PROCESS (c_err, s_cor_in, c_addb, c_specb)
		BEGIN
			IF (c_err = '0')
			THEN -- no error
				s_cor_out <= s_cor_in;
			ELSE -- error: CORRECTION WITHOUT CHECK
				s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)+1);
			END IF;
		END PROCESS;
	END GENERATE;

	-- '1' guess - always -1 correction
	dir_1: IF GUESS = '1' GENERATE
		PROCESS (c_err, s_cor_in, c_addb, c_specb)
		BEGIN
			IF (c_err = '0')
			THEN -- no error
				s_cor_out <= s_cor_in;
			ELSE -- error: CORRECTION WITHOUT CHECK
				s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)-1);
			END IF;
		END PROCESS;
	END GENERATE;

	-- variable guess
	dir_v: IF (GUESS /= '0' AND GUESS /= 'G' AND GUESS /= '1') GENERATE
		PROCESS (c_err, s_cor_in, c_addb, c_specb)
		BEGIN
			IF (c_err = '0')
			THEN -- no error
				s_cor_out <= s_cor_in;
			ELSE -- error: CORRECTION WITHOUT CHECK
				IF c_addb = '1'
				THEN -- too low error
					s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)+1);
				ELSE -- too high error
					s_cor_out <= STD_LOGIC_VECTOR(UNSIGNED(s_cor_in)-1);
				END IF;
			END IF;
		END PROCESS;
	END GENERATE;

END ARCHITECTURE standard;
