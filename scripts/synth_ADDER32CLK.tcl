# Copyright 2014 Vincent Camus all rights reserved
#
# EPFL, ICLAB     http://iclab.epfl.ch/approximate
# Vincent Camus   vincent.camus@epfl.ch
#
# Friday, May 16 2014
# Version 1.0


######### PREPARE THE ENVIRONMENT #########

# don't forget to set the different paths
set SOURCE_PATH source
set REPORT_PATH reports
set EXPORT_PATH exports

################# CLEAN-UP ################

remove_design -all

#sh rm -rf DLIB/WORK/*
#sh rm -rf WORK/*

sh mkdir -p $REPORT_PATH $EXPORT_PATH

################# ANALYZE #################

analyze  -format vhdl  -work work  $SOURCE_PATH/ISA_functions_pkg.vhd
analyze  -format vhdl  -work work  $SOURCE_PATH/ADD_stage.vhd
analyze  -format vhdl  -work work  $SOURCE_PATH/SPEC_stage.vhd
analyze  -format vhdl  -work work  $SOURCE_PATH/COMP_stage.vhd
analyze  -format vhdl  -work work  $SOURCE_PATH/ISA.vhd

# modify ISA parameters in the wrapper
analyze  -format vhdl  -work work  $SOURCE_PATH/ADDER32CLK_wrapper.vhd

################## DESIGN #################

elaborate ADDER32CLK

check_design -nosplit > $REPORT_PATH/report_check_ADDER32CLK.log
link
uniquify

############### CONSTRAINTS ###############

# choose the delay
create_clock -period 1.0 -name "clk" clk
set_ideal_network -no_propagate clk

set_max_area 0

################# COMPILE #################

compile_ultra

################# EXPORTS #################

change_names -rules verilog -hier
define_name_rules fixbackslashes -allowed "A-Za-z0-9_" -first_restricted "\\" -remove_chars
change_names -rule fixbackslashes -h

write -f verilog -hierarchy -output $EXPORT_PATH/ADDER32CLK.v
write -f ddc     -hierarchy -output $EXPORT_PATH/ADDER32CLK.ddc
write_sdf -version 2.1              $EXPORT_PATH/ADDER32CLK.sdf

################# REPORTS #################

report_area   -nosplit >  $REPORT_PATH/report_synth_ADDER32CLK.rpt
report_timing -nosplit >> $REPORT_PATH/report_synth_ADDER32CLK.rpt
report_power  -nosplit >> $REPORT_PATH/report_synth_ADDER32CLK.rpt

