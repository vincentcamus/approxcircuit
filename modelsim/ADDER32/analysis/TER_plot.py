#Function: plot the TER VS. ISA design 2D line graph 

import matplotlib.pyplot as plt 
from collections import defaultdict 
import re 
from decimal import * 
import numpy as np 

#return TER dictionary 
def Extract_TER(TER_list):
   TER_dict = defaultdict(float)
   TER_design_dict = defaultdict(list)
   TER_clk_dict = defaultdict(list)
   for TER_line in TER_list:     
      ISA = re.search('(.+?)-cu',TER_line.split()[-2]).group(1)
      CLK = re.search('-cu.(.+?).csv',TER_line.split()[-2]).group(1)
      TER = round(float(TER_line.split()[-1]),4)
      TER_dict[(ISA,CLK)] = TER
      TER_design_dict[ISA].append(TER)
      TER_clk_dict[CLK].append((ISA,TER))
   return TER_dict, TER_design_dict, TER_clk_dict

#return a list sorted by ISA 
def sorted_dict_ISA(TER_clk_dict):
    ISA_TER = np.zeros((len(TER_clk_dict.keys()), len(TER_clk_dict.values()[0])))
    #ISA_TER = []
    for n, k in enumerate(TER_clk_dict.keys()):
        temp = []
        for ISA, TER in sorted(TER_clk_dict[k], key = lambda x: x[0]):
            #temp.append((ISA,TER))
            temp.append(TER)
        ISA_TER[n] = temp  
        #ISA_TER.append(temp)
    return ISA_TER

#return a sorted dictionary list by value 
def sort_dict(TER_dict):
    sorted_dict_list = [] 
    sorted_TER_file = open('../stats/sorted_silver_err_cycle.txt', 'w')
    for k, v in sorted(TER_dict.items(), key = lambda x: x[1]):
        sorted_dict_list.append(str(k) + ': ' + str(v) + '\n')
        sorted_TER_file.write(str(k) + ':' + str(v) + '\n') 
    return sorted_dict_list


TER_file = open('../stats/silver_err_cycle.txt','r')  
TER_list = TER_file.readlines() 

TER_dict = defaultdict(float)
TER_design_dict = defaultdict(list)
TER_clk_dict = defaultdict(list)

TER_dict, TER_design_dict, TER_clk_dict = Extract_TER(TER_list)
ISA_TER = sorted_dict_ISA(TER_clk_dict)

#draw 2D line 
x_list = [v+1 for v in range(13)]
color_list = ['go-','bo-','ro-']
clk_list = ['5%', '10%', '15%']
for i in range(3):
    plt.plot(x_list, ISA_TER[i], color_list[i], label=clk_list[i])
plt.xticks(np.arange(min(x_list), max(x_list)+1))
plt.xlabel('ISA Design')
plt.ylabel('Timing Error Rate')
plt.legend(bbox_to_anchor=(0.91, 0.9), bbox_transform=plt.gcf().transFigure)
plt.show()






