#Function:
# 1) display erroneous cycle in golden and silver 
# 2) generate bit-level golden error file and silver error file 

from collections import defaultdict
import glob 

#return erroneous cycles 
def compute_err_cycle(a_list, b_list):
   err_cycle = 0 
   for a, b in zip(a_list, b_list):
       if a != b: 
           err_cycle += 1 
   return err_cycle 

#return bit level error count and write to file 
def compute_bit_err(a_list, b_list):
   bit_err = defaultdict(int)
   bit_len = len(a_list[0].rstrip())
   for a, b in zip(a_list, b_list):
       for i in range(bit_len):
           bit_err[bit_len-i] += (a[i] != b[i])
   bit_err_file = open('../stats/bit_err' + filename + post_cons,'w')
   for k, v in bit_err.items():
       bit_err_file.write(str(k) + ': ' + str(v/float(len(a_list))) + '\n')
   bit_err_file.close()
   return bit_err      

#return True if there is "X" in the output
def check_X(silver_list):
   for v in silver_list[2:]:
       if 'x' in v: 
           return True 
   return False

#return the files in a dir with pre and post constraints 
def find_filelist(pre_cons, post_cons):
   filename_list = [v[len(pre_cons):-len(post_cons)]  for v in glob.glob(pre_cons + "*" + post_cons)]
   return filename_list

#return the timing error file and write to file 
def generate_TER(filename, post_cons, golden_list, silver_list):
   TER_file = open('../TER_REP/TER.' + filename + post_cons, 'w')
   #TER_list = [format(int(a,2) ^ int(b,2),'033b') for a,b in zip(golden_list[])]
   for golden_elem, silver_elem in zip(golden_list, silver_list):
       TER = '' 
   #    print golden_elem, silver_elem
       for golden_bit, silver_bit in zip(golden_elem, silver_elem):
           TER +=  str(int(golden_bit == silver_bit))
       TER_file.write(TER + '\n')
   TER_file.close()
   return TER 
           

#define parameters 
pre_cons = '../REP/diamond.'
post_cons = '.csv'
   
golden_err_cycle_file = open('../stats/golden_err_cycle.txt','w')
silver_err_cycle_file = open('../stats/silver_err_cycle.txt','w')
golden_err_bit_file = open('../stats/golden_err_bit.txt','w')
silver_err_bit_file = open('../stats/silver_err_bit.txt', 'w')

for filename in find_filelist(pre_cons, post_cons):
   diamond_file = open("../REP/diamond." + filename + post_cons)
   golden_file = open("../REP/golden." + filename + post_cons)
   silver_file = open("../REP/silver." + filename + post_cons)

   diamond_list = diamond_file.readlines()
   golden_list = golden_file.readlines()
   silver_list = silver_file.readlines()
   list_len = float(len(golden_list))

   #TER_file_generate = generate_TER(filename, post_cons, golden_list, silver_list)
   #print filename + ': ' + str(check_X(silver_list))

   golden_err_cycle_file.write('Struct Error ' + filename + post_cons + ': ' + str(compute_err_cycle(diamond_list, golden_list)/list_len) + '\n')
   silver_err_cycle_file.write("Timing Error " + filename + post_cons + ': ' + str(compute_err_cycle(golden_list, silver_list)/list_len) + '\n')
   golden_err_bit_file.write('Struct Bit Error' + filename + post_cons +': ' + str(compute_bit_err(diamond_list, golden_list)) + '\n')
   silver_err_bit_file.write('Timing Bit Error' + filename + post_cons +': ' + str(compute_bit_err(golden_list, silver_list)) + '\n')
   
	 
###################### TRASH CODE ####################
#print filename_list
#print compute_err_cycle(golden_list, silver_list)
#print compute_bit_err(diamond_list, golden_list)
#print filename, compute_bit_err(golden_list, silver_list)
