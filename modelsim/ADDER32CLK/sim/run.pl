#Function: run ISA gate-level simulation 
#clock period: how much clock period we want to run the netlist 
#pre-requisite: 
#1) ADD ps TO the post-synthesis module 

use File::Copy;
#$dname = "IntAdder_32_f400_uid2_Wrapper";
$dname = "ADDER32CLK";
@clock_list = ("0.9");
#@clock_list = ("1.0", "0.9");

################## COMPILE FIXED SOURCE FILES #################################
system("vcom  -work work  ../../../source/ISA_functions_pkg.vhd");
system("vcom  -work work  ../../../source/ADD_stage.vhd");
system("vcom  -work work  ../../../source/SPEC_stage.vhd");
system("vcom  -work work  ../../../source/COMP_stage.vhd");
system("vcom  -work work  ../../../source/ISA.vhd");
system("vcom  -work work  ../../../source/ADDER32CLK_wrapper.vhd");
system("vlog  -quiet -93 -work work ../../../PS/$dname.ps.v");
system("vlog -quiet -93 -work work ../testbench/tb.v");

################## EXECUTE DIFFERENT DATA POINTS #############################
foreach $clock_period (@clock_list)
{
    system("rm -rf configure.txt");
    system("echo $clock_period >> configure.txt");
    system("vsim -c -sdfnoerror -sdfnowarn +no_tchk_msg -sdfmax tb/UUT2=../../../SDF/$dname.ps.sdf work.tb -do \"set StdArithNoWarnings 1; run -all\"");
    #system("mv ../REP/struct_err.txt ../REP/struct_err.$clock_period.txt");
    #system("mv ../REP/struct_time_err.txt ../REP/struct_time_err.$clock_period.txt");
}

################### TRASH CODE ###############################
# simulate with GUI 
#system("vsim work.tb");
#system("vsim -c work.tb -do \" set StdArithNoWarnings 1; run -all\"");

