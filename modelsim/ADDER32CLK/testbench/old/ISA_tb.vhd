-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus@epfl.ch
--
-- Friday, January 31 2014
-- Version 1.0


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;
USE std.textio.all;
USE ieee.std_logic_textio.all;

--------------------------------------------

ENTITY ISA_tb IS
END ISA_tb;

--------------------------------------------

ARCHITECTURE bench OF ISA_tb IS


---------------------------------- TB PARAMETERS

	-- modify testbench parameters here
	CONSTANT CLK_PER      : TIME    := 1 ns;
	CONSTANT ADDER_WIDTH  : INTEGER := 32;
	CONSTANT STIMULI_FILE : STRING  := "stimuli.csv";
	CONSTANT RESULTS_FILE : STRING  := "results.csv";

---------------------------------- ISA PARAMETERS

	-- modify ISA parameters here
	CONSTANT BLOCK_NB  : INTEGER := 8;
	CONSTANT ADD_REG   : STRING  := "45643244"; -- ADD sizes       0-9,a-z,A-Y and Z=64 !!!
	CONSTANT SPEC_REG  : STRING  := "-1232100"; -- SPEC sizes      0-9,a-z,A-Y
	CONSTANT GUESS_REG : STRING  := "-01ABPX-"; -- SPEC guesses    0,1,A,B,G,P,X,C,-
	CONSTANT TEST_REG  : STRING  := "-SSSSSSS"; -- COMP types      S,N
	CONSTANT COR_REG   : STRING  := "-1230000"; -- COMP cor sizes  0-9,a-z,A-Y
	CONSTANT RED_REG   : STRING  := "-0032100"; -- COMP red sizes  0-9,a-z,A-Y
	
---------------------------------- COMPONENTS

	COMPONENT ISA is
	GENERIC (
		ADDER_WIDTH : INTEGER; -- total bit-width of the ISA
		BLOCK_NB    : INTEGER; -- number of blocks of the ISA
		ADD_REG     : STRING;  -- ADD sizes                      char sizes: 0-9,a-z,A-Y=0..60 and Z=64 !!!
		SPEC_REG    : STRING;  -- SPEC sizes                     char sizes: 0-9,a-z,A-Y=0..60
		GUESS_REG   : STRING;  -- SPEC guesses                   char types: 0,1,A,B,G,P,X,C,-
		TEST_REG    : STRING;  -- COMP types                     char types: S=standard,N=no-test
		COR_REG     : STRING;  -- COMP correction sizes          char sizes: 0-9,a-z,A-Y=0..60
		RED_REG     : STRING); -- COMP reduction/balancing sizes char sizes: 0-9,a-z,A-Y=0..60
	PORT (
		a, b   : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cin    : IN  STD_LOGIC;
		s      : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cout   : OUT STD_LOGIC);
	END COMPONENT ISA;

---------------------------------- SIGNALS

	SIGNAL a        : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
	SIGNAL b        : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
	SIGNAL s_approx : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	SIGNAL s_exact  : STD_LOGIC_VECTOR (ADDER_WIDTH DOWNTO 0);
	SIGNAL temp     : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
	SIGNAL temp2    : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);

	-- carry-in not used in testbench
	SIGNAL c_in     : STD_LOGIC := '0';

---------------------------------- DESCRIPTION

BEGIN
	
	-- approximate addition
	UUT: ISA
	GENERIC MAP ( ADDER_WIDTH, BLOCK_NB, ADD_REG, SPEC_REG, GUESS_REG, TEST_REG, COR_REG, RED_REG )
	PORT MAP ( a, b, c_in, s_approx(ADDER_WIDTH-1 DOWNTO 0), s_approx(ADDER_WIDTH) );
	
	-- exact addition
	s_exact <= STD_LOGIC_VECTOR(UNSIGNED('0' & a)+UNSIGNED('0' & b));

	-- benchmark
	PROCESS

		-- variables
		VARIABLE line_stim : LINE;
		VARIABLE line_i    : INTEGER;
		VARIABLE line_a    : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		VARIABLE line_b    : STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		VARIABLE line_c    : CHARACTER;
		VARIABLE line_res  : LINE;

		-- files
		FILE fp_stim       : TEXT;
		FILE fp_res        : TEXT;

	BEGIN
	
		-- open all files
		file_open(fp_stim, STIMULI_FILE, READ_MODE);
		file_open(fp_res,  RESULTS_FILE,  WRITE_MODE);

		-- process
		WHILE NOT endfile(fp_stim) LOOP
			-- reading
			readline (fp_stim,line_stim);
			read(line_stim, line_i); -- line number
			read(line_stim, line_c); -- skip comma
			read(line_stim, line_a); -- a
			read(line_stim, line_c); -- skip comma
			read(line_stim, line_b); -- b
			-- applying stimulus
			a <= line_a;
			b <= line_b;
			-- establish signals
			WAIT FOR clk_per;
			-- assert and write error
			IF s_approx /= s_exact THEN
				ASSERT s_approx = s_exact;
				write(line_res, line_i, LEFT, 11); write (line_res, ',', LEFT, 1);
				write(line_res, s_approx, LEFT, ADDER_WIDTH+1); write (line_res, ',', LEFT, 1);
				write(line_res, s_exact,  LEFT, ADDER_WIDTH+1); write (line_res, HT,  LEFT, 1);
				writeline(fp_res,line_res);
			END IF;
			WAIT FOR clk_per;
		END LOOP;

		-- closing all files
		file_close(fp_stim);
		file_close(fp_res);

		-- end simulation
		assert false report "Simulation completed! Check 'results.csv' file." severity failure;
		
	END PROCESS;

END ARCHITECTURE bench;
